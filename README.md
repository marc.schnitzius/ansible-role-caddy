# Caddy ansible role

This role deploys [caddy](https://caddyserver.com/) as an reverse-proxy in a docker-compose stack and let's you can configure multiple http backends.

## Variables

```yaml
domains:
  - name: domain1.com
    backend: https://backend1:8181
    tls_insecure: true
  - name: domain2.com
    backend: https://backend2:8181
```
## Logging
This role creates a separate log file for each domain in the `/var/log/caddy/` directory. To persist these logs, mount a Docker volume or a host system path to this directory.

The role also provides configuration options for log rotation with sensible defaults:

```yaml
roll_size: 100MiB  # Maximum size of a log file before it gets rotated
roll_keep: 10      # Number of rotated log files to keep
roll_keep_for: 720h  # Age to keep rotated log files (30d)
```

For more information read the [docs](https://caddyserver.com/docs/caddyfile/directives/log#file)

## pre-commit hooks

[Pre-commit hooks](https://pre-commit.com/) are used to automatically enforce a project's coding standards and reduce the chance of committing common coding errors or any code that doesn't adhere to the project's guidelines.

****trailing-whitespace:**** This hook trims trailing whitespace.

****end-of-file-fixer:**** This hook ensures that a file is either empty, or ends with one newline.

**check-yaml:** This hook attempts to load all yaml files to verify syntax.

**check-added-large-files:** This hook prevents giant files from being committed.

**ansible-lint:** This hook runs ansible-lint on the entire codebase.

**yamllint:** This hook runs yamllint to check for syntax validity and key repetition.

**detect-secrets:** This hook runs detect-secrets to prevent committing secrets into the repository.

### Setup

```shell
pip install pre-commit detect-secrets
pre-commit install
```

## molecule tests

### Setup
```shell
pip install molecule-docker
```

#### Mac
```shell
sudo ln -s -f /Users/$USER/.docker/run/docker.sock /var/run/docker.sock
```

### Run
```shell
molecule test
```

The pre_build_image: true setting in the molecule/default/molecule.yml file is used to speed up Molecule tests. When this setting is enabled, Molecule will not rebuild the Docker image before each test run. Instead, it will use the existing Docker image.
